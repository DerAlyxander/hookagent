package dev.dersteffen.hookagent.mappings;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor @NoArgsConstructor
public class MethodMapping {

	private String name;
	private String obfName;
	private String signature;
	
}
