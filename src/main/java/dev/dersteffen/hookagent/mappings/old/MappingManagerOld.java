package dev.dersteffen.hookagent.mappings.old;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

@Deprecated
public enum MappingManagerOld {
	ME;
	
	public enum MappingType { CLASS, FIELD, METHOD }
	public enum CheckType { OBF, UNOBF }
	
	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	private static final List<ClassMappingOld> classMappings = new ArrayList<ClassMappingOld>();
	
	public static void load(String pPath, String pVersion) {
		File mappingsFolder = new File(new File(pPath), pVersion);
		if (!mappingsFolder.exists()) return;
		Arrays.asList(mappingsFolder.listFiles(file -> file.getPath().endsWith(".json"))).forEach(file -> {
			try {
				JsonReader reader = new JsonReader(new BufferedReader(new FileReader(file)));
				ClassMappingOld classMapping = GSON.fromJson(reader, ClassMappingOld.class);
				classMapping.setUnObfName(file.getName().substring(0, file.getName().lastIndexOf(".")));
				classMappings.add(classMapping);
			} catch (Exception ex) { ex.printStackTrace(); }
		});
	}
	
	public String getObfName(String pClassName) {
		return this.getObfName(pClassName, null, MappingType.CLASS);
	}
	
	public String getObfName(String pClassName, String pUnObfName, MappingType pMappingType) {
		try {
			switch (pMappingType) {
				case CLASS:
					return Optional.ofNullable(this.getMappingByClassName(pClassName, CheckType.UNOBF).getObfName()).orElse("");
				case FIELD:
					return Optional.ofNullable(this.getMappingByClassName(pClassName, CheckType.UNOBF).getFields().get(pUnObfName)).orElse("");
				case METHOD:
					return Optional.ofNullable(this.getMappingByClassName(pClassName, CheckType.UNOBF).getMethodMapping(pUnObfName)).orElse(null).getObfName();
				default:
					return "";
			}
		} catch (Exception ex) {
			return "";
		}
	}
	
	public boolean isMapped(String pName, CheckType pCheckType) {
		return this.getMappingByClassName(pName, pCheckType) != null;
	}
	
	public ClassMappingOld getMappingByClassName(String pName, CheckType pCheckType) {
		switch (pCheckType) {
			case OBF:
				return classMappings.stream().filter(classMapping -> classMapping.getObfName().equalsIgnoreCase(pName)).findFirst().orElse(null);
			case UNOBF:
				return classMappings.stream().filter(classMapping -> classMapping.getUnObfName().equalsIgnoreCase(pName)).findFirst().orElse(null);
			default: return null;
		}
	}
	
}
