package dev.dersteffen.hookagent.mappings.old;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ClassMappingOld {
	
	private transient String unObfName;
	private String obfName;
			
	private List<MethodMappingOld> methods = new ArrayList<MethodMappingOld>();
	private Map<String, String> fields = new HashMap<String, String>();
	
	public ClassMappingOld() {}
	
	public ClassMappingOld(String pUnObfName, String pObfName) {
		this.unObfName = pUnObfName;
		this.obfName = pObfName;
	}
	
	public MethodMappingOld getMethodMapping(String pUnObfName) {
		return this.methods.stream().filter(mapping -> mapping.getUnObfName().equalsIgnoreCase(pUnObfName)).findFirst().orElse(null);
	}
	
	@Override
	public String toString() {
		return this.unObfName + " | " + this.obfName;
	}
	
}
