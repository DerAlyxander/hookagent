package dev.dersteffen.hookagent.mappings.old;

public class MethodMappingOld {

	private String unObfName;
	private String obfName;
	private String signature;
	
	public MethodMappingOld() {}
	
	public MethodMappingOld(String pUnObfName, String pObfName, String pSignature) {
		this.unObfName = pUnObfName;
		this.obfName = pObfName;
		this.signature = pSignature;
	}
	
	public String getUnObfName() { return this.unObfName; }
	
	public String getObfName() { return this.obfName; }
	
	public String getSignature() { return this.signature; }
	
}
