package dev.dersteffen.hookagent;

public class Logging {

	private Logging() {}
	
	public static void modifyClass(String pClassName, String pObfClassName) {
		System.out.println("[Injecti0n] Hooking class '%obfclass% <%class%>'"
				   			.replace("%class%", pClassName)
				   			.replace("%obfclass%", pObfClassName));
	}
	
	public static void generateGetter(String pFieldName, String pObfFieldName) {
		System.out.println("\t\tCreating Getter for field '%obffield% <%field%>'"
				   			.replace("%field%", pFieldName)
				   			.replace("%obffield%", pObfFieldName));
	}
	
	public static void generateSetter(String pFieldName, String pObfFieldName) {
		System.out.println("\t\tCreating Setter for field '%obffield% <%field%>'"
				   .replace("%field%", pFieldName)
				   .replace("%obffield%", pObfFieldName));
	}
	
	public static void generateMethod(String pMethodName, String pObfMethodName) {
		System.out.println("\t\tCreating delegate-method for method '%obfmethod% <%method%>'"
				   .replace("%method%", pMethodName)
				   .replace("%obfmethod%", pObfMethodName));
	}
	
	public static void generateMethodMixin(String pMethodName, String pTargetMethodName) {
		System.out.println("\t\tInjecting method '%method%' to '%targetmethod%'"
	   						.replace("%method%", pMethodName)
	   						.replace("%targetmethod%", pTargetMethodName));
	}
	
}
