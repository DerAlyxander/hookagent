package dev.dersteffen.hookagent.eventsystem;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class EventManager {

	private static final List<Listener> listeners = new ArrayList<Listener>();

	public static void addListener(Listener listener) { listeners.add(listener); }

	public static void callEvent(Event pEventToCall) {
		try {
			for (Listener listener : listeners) {
				for (Method method : listener.getClass().getDeclaredMethods()) {
					method.setAccessible(true);

					if (!method.isAnnotationPresent(EventHandler.class)) continue;
					if (method.getParameterCount() != 1) continue;
					if (method.getParameterTypes()[0] != pEventToCall.getClass()) continue;

					method.invoke(listener, pEventToCall);
				}
			}
		} catch (Exception ex) { ex.printStackTrace(); }
	}

}
