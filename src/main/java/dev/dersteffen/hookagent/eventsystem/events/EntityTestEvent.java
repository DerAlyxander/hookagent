package dev.dersteffen.hookagent.eventsystem.events;

import dev.dersteffen.hookagent.eventsystem.Event;
import dev.dersteffen.hookagent.mcserver.facades.BlockPosition;
import dev.dersteffen.hookagent.mcserver.facades.EntityPlayer;

public class EntityTestEvent implements Event {

	private EntityPlayer entity;
	private BlockPosition blockPosition;
	
	public EntityTestEvent(EntityPlayer pEntity, BlockPosition pBlockPosition) {
		this.entity = pEntity;
		this.blockPosition = pBlockPosition;
	}
	
	public EntityPlayer getEntity() { return this.entity; }
	
	public BlockPosition getBlockPosition() { return this.blockPosition; }
	
}
