package dev.dersteffen.hookagent.eventsystem.events;

import dev.dersteffen.hookagent.eventsystem.Event;
import dev.dersteffen.hookagent.mcserver.facades.Block;
import dev.dersteffen.hookagent.mcserver.facades.BlockPosition;
import dev.dersteffen.hookagent.mcserver.facades.EntityPlayer;
import dev.dersteffen.hookagent.mcserver.facades.World;

public class PostRightClickBlockEvent implements Event {

	private EntityPlayer entity;
	private Block block;
	private BlockPosition blockPosition;
	
	public PostRightClickBlockEvent(EntityPlayer pEntity, World pWorld, BlockPosition pBlockPosition) {
		this.entity = pEntity;
		this.block = pWorld.getBlockState(pBlockPosition).getBlock();
		this.blockPosition = pBlockPosition;
	}
	
	public EntityPlayer getEntity() { return this.entity; }
	
	public Block getBlock() { return this.block; }
	
	public BlockPosition getBlockPosition() { return this.blockPosition; }
	
}
