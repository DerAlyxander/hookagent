package dev.dersteffen.hookagent.listeners;

import dev.dersteffen.hookagent.eventsystem.EventHandler;
import dev.dersteffen.hookagent.eventsystem.Listener;
import dev.dersteffen.hookagent.eventsystem.events.RightClickBlockEvent;

public class TestListener implements Listener {

	@EventHandler
	public void onTest(RightClickBlockEvent pClickEvent) {
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("----INFO----");
		System.out.println("Playername: " + pClickEvent.getEntity().getGameProfile().getName());
		System.out.println("Blockname: " + pClickEvent.getBlock().getUnlocalizedName());
		System.out.println("Player [x - y - z]: [" + pClickEvent.getEntity().getPosX() + " - " + pClickEvent.getEntity().getPosY() + " - " + pClickEvent.getEntity().getPosZ() + "]");
		System.out.println("ClickedBlock [x - y - z]: [" + pClickEvent.getBlockPosition().getX() + " - " + pClickEvent.getBlockPosition().getY() + " - " + pClickEvent.getBlockPosition().getZ() + "]");
		System.out.println("------------");
	}
	
}
