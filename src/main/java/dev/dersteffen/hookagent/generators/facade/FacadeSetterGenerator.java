package dev.dersteffen.hookagent.generators.facade;

import java.lang.reflect.Method;

import dev.dersteffen.hookagent.Logging;
import dev.dersteffen.hookagent.annotations.facade.ClassFacade;
import dev.dersteffen.hookagent.annotations.facade.SetterFacade;
import dev.dersteffen.hookagent.generators.IGenerator;
import dev.dersteffen.hookagent.mappings.old.MappingManagerOld;
import dev.dersteffen.hookagent.mappings.old.MappingManagerOld.MappingType;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.CtNewMethod;
import javassist.NotFoundException;

public class FacadeSetterGenerator implements IGenerator<Method> {

	@Override
	public void generate(ClassPool pClassPool, Method pMethod, CtClass pCtClass) throws Exception {
		SetterFacade injectSetter = pMethod.<SetterFacade>getAnnotation(SetterFacade.class);
		if (injectSetter.dontCreate()) return;
		
		String externalClassName = pMethod.getDeclaringClass().getAnnotation(ClassFacade.class).value();
		
		String fieldName = injectSetter.value();
		fieldName = MappingManagerOld.ME.getObfName(externalClassName, fieldName, MappingType.FIELD);		
		
		CtField getField;
		try {
			getField = pCtClass.getDeclaredField(fieldName);
		} catch (NotFoundException ex) { return; }
		
		Logging.generateSetter(injectSetter.value(), fieldName);
		CtMethod setterMethod = CtNewMethod.setter(pMethod.getName(), getField);
		pCtClass.addMethod(setterMethod);
	}

}
