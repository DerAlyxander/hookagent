package dev.dersteffen.hookagent.generators.mixin;

import java.lang.reflect.Method;

import org.objectweb.asm.Type;

import dev.dersteffen.hookagent.Logging;
import dev.dersteffen.hookagent.annotations.mixin.ClassMixin;
import dev.dersteffen.hookagent.annotations.mixin.MethodMixin;
import dev.dersteffen.hookagent.annotations.mixin.MixinPoint;
import dev.dersteffen.hookagent.annotations.mixin.ParamToField;
import dev.dersteffen.hookagent.annotations.mixin.ParamToParam;
import dev.dersteffen.hookagent.generators.IGenerator;
import dev.dersteffen.hookagent.mappings.old.MappingManagerOld;
import dev.dersteffen.hookagent.mappings.old.MethodMappingOld;
import dev.dersteffen.hookagent.mappings.old.MappingManagerOld.CheckType;
import dev.dersteffen.hookagent.utils.U;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtNewMethod;

public class MixinMethodGenerator implements IGenerator<Method> {

	@Override
	public void generate(ClassPool pClassPool, Method pMethod, CtClass pCtClass) throws Exception {
		MethodMixin methodMixin = pMethod.getAnnotation(MethodMixin.class);
		String internalClassName = pMethod.getDeclaringClass().getAnnotation(ClassMixin.class).value();
		
		CtClass mixinClass = pClassPool.get(pMethod.getDeclaringClass().getName());
		CtMethod methodCopy = CtNewMethod.copy(mixinClass.getMethod(pMethod.getName(), Type.getMethodDescriptor(pMethod)), pCtClass, null);
		pCtClass.addMethod(methodCopy);
		
		String methodName = methodMixin.value();
		String methodSignature = methodMixin.signature();
		
		MethodMappingOld methodMapping = MappingManagerOld.ME.getMappingByClassName(internalClassName, CheckType.UNOBF).getMethodMapping(methodName);
		if (methodMapping != null) {
			methodName = methodMapping.getObfName();
			methodSignature = methodMapping.getSignature();
		}
		
		String methodString = U.createMethod(this.generateMethodCall(internalClassName, methodCopy, methodMixin.paramToField(), methodMixin.paramToParam()));
		
		Logging.generateMethodMixin(pMethod.getName(), methodName + " <" + methodSignature + ">");
		CtMethod targetMethod = pCtClass.getMethod(methodName, methodSignature);
		
		MixinPoint point = methodMixin.point();

		switch (point) {
			case AFTER:
				targetMethod.insertAfter(methodString);
				break;
			case BEFORE:
				targetMethod.insertBefore(methodString);
				break;
		}
	}
	
	private String generateMethodCall(String pInternalClassName, CtMethod pCtMethod, ParamToField[] pParamToFields, ParamToParam[] pParamToParams) throws Exception {
		String method = this.prepareMethod(pCtMethod);
		method = setupParamToField(pInternalClassName, method, pParamToFields);
		method = setupParamToParam(method, pParamToParams);
		return method;
	}
	
	public String prepareMethod(CtMethod pCtMethod) throws Exception {
		StringBuilder methodBuilder = new StringBuilder();
		methodBuilder.append("$0." + pCtMethod.getName() + "(");

		int parameterSize = pCtMethod.getParameterTypes().length;
		for (int i = 0; i < parameterSize; i++) {
			methodBuilder.append("%" + i + "%");
			if (i < parameterSize - 1) methodBuilder.append(",");
		}
		methodBuilder.append(");");
		
		return methodBuilder.toString();
	}
	
	public String setupParamToField(String pInternalClassName, String pMethodString, ParamToField[] pParamToFields) {
		for (ParamToField param : pParamToFields) {
			if (param.index() != -1) {
				String fieldName = MappingManagerOld.ME.getMappingByClassName(pInternalClassName, CheckType.UNOBF).getFields().get(param.name());
				pMethodString = pMethodString.replace("%" + param.index() + "%", "$0." + fieldName);
			}
		}
		
		return pMethodString;
	}
	
	public String setupParamToParam(String pMethodString, ParamToParam[] pParamToParams) {
		for (ParamToParam param : pParamToParams)
			if (param.index() != -1)
				pMethodString = pMethodString.replace("%" + param.index() + "%", "$" + param.targetIndex());
		
		return pMethodString;
	}

}
