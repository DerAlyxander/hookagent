package dev.dersteffen.hookagent.utils;

import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class U {

	private U() {}
	
	public static Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	
	public static String createMethod(String pContent) {
		StringBuilder methodString = new StringBuilder();
		methodString.append("{");
		methodString.append(pContent);
		methodString.append("}");
		return methodString.toString();
	}
	
	public static String createMethod(String[] pLines) {
		StringBuilder methodString = new StringBuilder();
		Arrays.asList(pLines).forEach(methodString::append);
		return createMethod(methodString.toString());
	}
	
	public static String createMethod(List<String> pLines) {
		return createMethod(pLines.stream().toArray(String[]::new));
	}
	
}
