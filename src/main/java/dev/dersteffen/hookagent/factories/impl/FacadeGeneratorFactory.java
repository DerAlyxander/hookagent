package dev.dersteffen.hookagent.factories.impl;

import java.lang.reflect.Method;

import dev.dersteffen.hookagent.annotations.facade.ClassFacade;
import dev.dersteffen.hookagent.annotations.facade.GetterFacade;
import dev.dersteffen.hookagent.annotations.facade.MethodFacade;
import dev.dersteffen.hookagent.annotations.facade.SetterFacade;
import dev.dersteffen.hookagent.factories.IGeneratorFactory;
import dev.dersteffen.hookagent.generators.IGenerator;
import dev.dersteffen.hookagent.generators.facade.FacadeGenerator;
import dev.dersteffen.hookagent.generators.facade.FacadeGetterGenerator;
import dev.dersteffen.hookagent.generators.facade.FacadeMethodGenerator;
import dev.dersteffen.hookagent.generators.facade.FacadeSetterGenerator;

public class FacadeGeneratorFactory implements IGeneratorFactory {
	
	private final FacadeGetterGenerator FACADE_GETTER_GENERATOR = new FacadeGetterGenerator();
	private final FacadeSetterGenerator FACADE_SETTER_GENERATOR = new FacadeSetterGenerator();
	private final FacadeMethodGenerator FACADE_METHOD_GENERATOR = new FacadeMethodGenerator();
	
	@Override
	public IGenerator<Method> select(Method pMethod) {
		if (pMethod.getAnnotation(GetterFacade.class) != null) {
			return this.FACADE_GETTER_GENERATOR;
		} else if (pMethod.getAnnotation(SetterFacade.class) != null) {
			return this.FACADE_SETTER_GENERATOR;
		} else if (pMethod.getAnnotation(MethodFacade.class) != null) {
			return this.FACADE_METHOD_GENERATOR;
		}
		
		return IGenerator.empty();
	}

	private final FacadeGenerator INTERFACE_GENERATOR = new FacadeGenerator();
	
	@Override
	public IGenerator<Class<?>> select(Class<?> pClass) {
		if (pClass.getAnnotation(ClassFacade.class) != null) {
			return this.INTERFACE_GENERATOR;
		}
		
		return IGenerator.empty();
	}
	
}
