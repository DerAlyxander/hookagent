package dev.dersteffen.hookagent.factories.impl;

import java.lang.reflect.Method;

import dev.dersteffen.hookagent.annotations.mixin.MethodMixin;
import dev.dersteffen.hookagent.factories.IGeneratorFactory;
import dev.dersteffen.hookagent.generators.IGenerator;
import dev.dersteffen.hookagent.generators.mixin.MixinMethodGenerator;

public class MixinGeneratorFactory implements IGeneratorFactory {

	private final MixinMethodGenerator MIXIN_METHOD_GENERATOR = new MixinMethodGenerator();
	
	@Override
	public IGenerator<Method> select(Method pMethod) {
		if (pMethod.getAnnotation(MethodMixin.class) != null) {
			return this.MIXIN_METHOD_GENERATOR;
		}
		
		return IGenerator.empty();
	}

	@Override
	public IGenerator<Class<?>> select(Class<?> pClass) {
		return IGenerator.empty();
	}

}
