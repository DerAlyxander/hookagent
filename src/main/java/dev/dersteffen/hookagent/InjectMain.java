package dev.dersteffen.hookagent;

import java.io.File;
import java.lang.instrument.Instrumentation;

import dev.dersteffen.hookagent.eventsystem.EventManager;
import dev.dersteffen.hookagent.listeners.TestListener;
import dev.dersteffen.hookagent.mappings.MappingManager;
import dev.dersteffen.hookagent.mappings.old.MappingManagerOld;
import dev.dersteffen.hookagent.transformer.InjectionManager;
import dev.dersteffen.hookagent.transformer.InjectionTransformer;

public class InjectMain {

	public static File MAPPINGS_FILE;
	
	public static final String JAR_PATH = InjectMain.class.getProtectionDomain().getCodeSource().getLocation().getFile();
	public static final File JAR_FILE = new File(JAR_PATH.substring(0, JAR_PATH.lastIndexOf("/")));

	public static final String VERSION = "1.13";
	
	public static void premain(String agentArgument, Instrumentation instrumentation) {
		MappingManagerOld.load(JAR_FILE.getAbsolutePath(), VERSION);
		EventManager.addListener(new TestListener());
		instrumentation.addTransformer(new InjectionTransformer(InjectionManager.ME, MappingManager.ME));
	}

}
