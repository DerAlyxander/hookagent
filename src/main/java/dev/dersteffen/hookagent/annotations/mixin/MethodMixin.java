package dev.dersteffen.hookagent.annotations.mixin;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(METHOD)
public @interface MethodMixin {

	String value();
		
	String signature() default "";
	
	MixinPoint point() default MixinPoint.BEFORE;
		
	ParamToField[] paramToField() default @ParamToField(index = -1, name = "");
	
	ParamToParam[] paramToParam() default @ParamToParam(index = -1, targetIndex = -1);

}
