package dev.dersteffen.hookagent.annotations.mixin;

public enum MixinPoint {
	BEFORE, AFTER;
}
