package dev.dersteffen.hookagent.annotations.facade;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(METHOD)
public @interface GetterFacade {

	String value();
	boolean dontCreate() default false;
	
}
