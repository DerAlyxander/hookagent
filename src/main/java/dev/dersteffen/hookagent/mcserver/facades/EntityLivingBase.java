package dev.dersteffen.hookagent.mcserver.facades;

import dev.dersteffen.hookagent.annotations.facade.ClassFacade;

@ClassFacade("EntityLivingBase")
public interface EntityLivingBase extends Entity {
	
}
