package dev.dersteffen.hookagent.mcserver.facades;

import dev.dersteffen.hookagent.annotations.facade.ClassFacade;

@ClassFacade("EntityPlayerMP")
public interface EntityPlayerMP extends EntityPlayer {
	
}
