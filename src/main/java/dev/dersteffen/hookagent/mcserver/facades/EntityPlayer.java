package dev.dersteffen.hookagent.mcserver.facades;

import dev.dersteffen.hookagent.annotations.facade.ClassFacade;
import dev.dersteffen.hookagent.annotations.facade.GetterFacade;

@ClassFacade("EntityPlayer")
public interface EntityPlayer extends EntityLivingBase {

	@GetterFacade("gameProfile")
	GameProfile getGameProfile();
	
}
