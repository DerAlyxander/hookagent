package dev.dersteffen.hookagent.mcserver.facades;

import dev.dersteffen.hookagent.annotations.facade.ClassFacade;
import dev.dersteffen.hookagent.annotations.facade.GetterFacade;
import dev.dersteffen.hookagent.annotations.facade.SetterFacade;

@ClassFacade("Entity")
public interface Entity {

	@GetterFacade("posX")
	double getPosX();
	
	@SetterFacade("posX")
	void setPosX(double posX);
	
	@GetterFacade("posY")
	double getPosY();
	
	@SetterFacade("posY")
	void setPosY(double posY);
	
	@GetterFacade("posZ")
	double getPosZ();
	
	@SetterFacade("posZ")
	void setPosZ(double posZ);
	
}
